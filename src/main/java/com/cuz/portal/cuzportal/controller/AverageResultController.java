package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.AverageResult;
import com.cuz.portal.cuzportal.repository.AverageResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AverageResultController {
    @Autowired
    private AverageResultRepository averageResultRepository;

    @GetMapping("/averageResults")
    public Iterable<AverageResult> getAllAverageResults(){
        return averageResultRepository.findAll();
    }
    @PostMapping("/averageResult")
    public AverageResult addAverageResult(@RequestBody AverageResult averageResult){
        AverageResult averageResult1 = averageResultRepository.save(averageResult);
        String averageResultId =  averageResult1.getId();

        return averageResultRepository.findById(averageResultId).get();
    }
    @GetMapping("/averageResult/{regNum}")
    public AverageResult getAverageResult(@PathVariable String regNum){
        return averageResultRepository.findByRegNum(regNum);
    }

}
