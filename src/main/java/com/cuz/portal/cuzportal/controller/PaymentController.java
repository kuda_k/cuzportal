package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.Payment;
import com.cuz.portal.cuzportal.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class PaymentController {
    @Autowired
    private PaymentRepository paymentRepository;

    @GetMapping("payments")
    public Iterable<Payment> getAllPayments(){
        return paymentRepository.findAll();
    }
    @GetMapping("/payments/{id}")
    public Payment getStudentPayment(@PathVariable("id")String id){
        return paymentRepository.findById(id).get();
    }
    @PostMapping("/payment")
    public Payment processPayment(@RequestBody Payment payment){
        Payment payment1 = new Payment();

        paymentRepository.save(payment1);
        String paymentId = payment1.getId();
        return paymentRepository.findById(paymentId).get();
    }
}
