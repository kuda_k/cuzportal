package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.Programme;
import com.cuz.portal.cuzportal.repository.ProgrammeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProgrammeController {
    @Autowired
    ProgrammeRepository programmeRepository;

    @GetMapping("/programmes")
    public Iterable<Programme> getAllProgrammes(){
        return programmeRepository.findAll();
    }
    @GetMapping("/programmes/{id}")
    public Programme getProgrammeById(@PathVariable("id") String id){
        return programmeRepository.findById(id).get();
    }
}
