package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.Course;
import com.cuz.portal.cuzportal.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CourseController {
    @Autowired
    private CourseRepository courseRepository;

    @GetMapping("/courses")
    public Iterable<Course> getAllCourses() {
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++start============================");
        return courseRepository.findAll();
    }

    @PostMapping("/course")
    public Course addCourse(@RequestBody Course course){
        courseRepository.save(course);
        Course course1 = courseRepository.save(course);
        String courseId = course1.getId();
        return courseRepository.findById(courseId).get();

    }

}
