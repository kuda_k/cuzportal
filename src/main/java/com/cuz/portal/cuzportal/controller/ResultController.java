package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.Result;
import com.cuz.portal.cuzportal.document.Student;
import com.cuz.portal.cuzportal.repository.ResultRepository;
import com.cuz.portal.cuzportal.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api")
public class ResultController {

    @Autowired
    private ResultRepository resultRepository;
    @Autowired
    private StudentRepository studentRepository;
@GetMapping("/results")
    public List<Result> getAllStudentsResults(){
        return resultRepository.findAll();

    }
    @RequestMapping("/results/{id}")
    public Iterable<Result> getStudentResult(@PathVariable("id") String id){

    Student student = studentRepository.findById(id).get();
    Iterable<Result> results = resultRepository.findResultsByStudent(student);
    return results;
    }
     //save result or create result
    @PostMapping("/results")
    public String addStudentResult(@RequestBody Result result){
     resultRepository.save(result);
    return "saved";
    }
}
