package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.User;
import com.cuz.portal.cuzportal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/signup")
    public User addUser(@RequestBody User user){
        return userRepository.save(user);
    }

    @GetMapping("/users")
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }
    @DeleteMapping("/deleteAll")
    public void deleteAll(){
        userRepository.deleteAll();
    }

    @GetMapping("/profile/{regNum}")
    public User getStudentProfile(@PathVariable("regNum") String regNum){
       return userRepository.findUserByUserName(regNum);

    }




}
