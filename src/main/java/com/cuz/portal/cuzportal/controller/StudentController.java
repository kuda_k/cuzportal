package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.Student;
import com.cuz.portal.cuzportal.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class StudentController {

   @Autowired
private StudentRepository studentRepository;
      @GetMapping("/students")
    public Iterable<Student> getAllStudents(){
        return studentRepository.findAll();
    }
    @GetMapping("/student/{id}")
    public Student getStudentById(@PathVariable("id") String id)
    {
        return studentRepository.findById(id).get();
    }

    @PostMapping("/student")
          public Student addStudent(@RequestBody Student student){
            Student student1 = studentRepository.save(student);
             String studentId =  student1.getId();
             return studentRepository.findById(studentId).get();
        }

}