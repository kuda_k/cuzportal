package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.DeanOfStudents;
import com.cuz.portal.cuzportal.document.Lecturer;
import com.cuz.portal.cuzportal.document.Student;
import com.cuz.portal.cuzportal.document.User;
import com.cuz.portal.cuzportal.repository.DeanOfStudentsRepository;
import com.cuz.portal.cuzportal.repository.LecturerRepository;
import com.cuz.portal.cuzportal.repository.StudentRepository;
import com.cuz.portal.cuzportal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LoginController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private LecturerRepository lecturerRepository;
    @Autowired
    private DeanOfStudentsRepository deanOfStudentsRepository;


    @GetMapping("/login")
    public ResponseEntity<User> login(@RequestParam("username") String userName, @RequestParam("password") String password) {


        User user = userRepository.findUserByUserName(userName);


        HttpHeaders header = new HttpHeaders();
        header.add("Responded", "userExists");
        header.add("Role",user.getRole());


        if (user != null) {
            if (userRepository.findByUserNameAndPassword(userName,password) != null) {

                return new ResponseEntity<>(user, header, HttpStatus.OK);
            }
            else
                return null;
        }
        else
            return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
    }

}