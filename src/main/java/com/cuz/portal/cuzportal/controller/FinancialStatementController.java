package com.cuz.portal.cuzportal.controller;

import com.cuz.portal.cuzportal.document.FinancialStatement;
import com.cuz.portal.cuzportal.document.Student;
import com.cuz.portal.cuzportal.repository.FinancialStatementRepository;
import com.cuz.portal.cuzportal.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FinancialStatementController {

    @Autowired
    private FinancialStatementRepository financialStatementRepository;
    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/fnstatements")
    public Iterable<FinancialStatement> getAllStudentsFinancialStatements(){
        return financialStatementRepository.findAll();
    }
    @GetMapping("/fnstatements/{id}")
    public FinancialStatement getStudentFinancialStatement(@PathVariable("id")String id){
        Student student = studentRepository.findById(id).get();
        return financialStatementRepository.findFinancialStatementByStudent(student);
    }
}
