package com.cuz.portal.cuzportal.service;

import com.cuz.portal.cuzportal.document.Course;

public interface CourseService extends IService<Course> {
}
