package com.cuz.portal.cuzportal.service;

import java.util.List;
import java.util.Optional;

public interface IService<T> {
    T save(T t);
    Optional<T> findById(String id);
    Optional<List<T>>findAll();
    void delete(String id);


    default T findByName(String name){
        return (T) name;
    }
}
