package com.cuz.portal.cuzportal.service;

import com.cuz.portal.cuzportal.document.Parent;

public interface ParentService extends IService<Parent> {
}
