package com.cuz.portal.cuzportal.service;

import com.cuz.portal.cuzportal.document.Programme;

public interface ProgrammeService extends IService<Programme> {
}
