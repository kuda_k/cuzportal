package com.cuz.portal.cuzportal.serviceImp;

import com.cuz.portal.cuzportal.document.Parent;
import com.cuz.portal.cuzportal.repository.ParentRepository;
import com.cuz.portal.cuzportal.service.ParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParentServiceImp implements ParentService {
    @Autowired
    ParentRepository parentRepository;

    @Override
    public Parent save(Parent parent) {
        return parentRepository.save(parent);
    }

    @Override
    public Optional<Parent> findById(String id) {
        return Optional.ofNullable(parentRepository.findById(id).get());
    }

    @Override
    public Optional<List<Parent>> findAll() {
        return Optional.ofNullable(parentRepository.findAll());
    }

    @Override
    public void delete(String id) {
  parentRepository.deleteById(id);
    }
}
