package com.cuz.portal.cuzportal.serviceImp;


import com.cuz.portal.cuzportal.document.Course;
import com.cuz.portal.cuzportal.repository.CourseRepository;
import com.cuz.portal.cuzportal.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImp implements CourseService {
    @Autowired
    CourseRepository courseRepository;

    @Override
    public Course save(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public Optional<Course> findById(String id) {
        return Optional.ofNullable(courseRepository.findById(id).get());
    }

    @Override
    public Optional<List<Course>> findAll() {
        return Optional.ofNullable(courseRepository.findAll());
    }

    @Override
    public void delete(String id) {
    courseRepository.deleteById(id);
    }
}
