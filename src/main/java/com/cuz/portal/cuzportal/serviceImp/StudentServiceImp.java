package com.cuz.portal.cuzportal.serviceImp;

import com.cuz.portal.cuzportal.document.Student;
import com.cuz.portal.cuzportal.repository.StudentRepository;
import com.cuz.portal.cuzportal.service.StundentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImp implements StundentService {
    @Autowired
    StudentRepository studentRepository;


    @Override
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Optional<Student> findById(String id) {
        return Optional.ofNullable(studentRepository.findById(id).get());
    }

    @Override
    public Optional<List<Student>> findAll() {
        return Optional.ofNullable(studentRepository.findAll());
    }

    @Override
    public void delete(String id) {
   studentRepository.deleteById(id);
    }
}
