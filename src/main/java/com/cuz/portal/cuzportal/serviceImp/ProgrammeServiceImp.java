package com.cuz.portal.cuzportal.serviceImp;

import com.cuz.portal.cuzportal.document.Programme;
import com.cuz.portal.cuzportal.repository.ProgrammeRepository;
import com.cuz.portal.cuzportal.service.ProgrammeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProgrammeServiceImp implements ProgrammeService {
    @Autowired
    ProgrammeRepository programmeRepository;

    @Override
    public Programme save(Programme programme) {
        return programmeRepository.save(programme);
    }

    @Override
    public Optional<Programme> findById(String id) {
        return Optional.ofNullable(programmeRepository.findById(id).get());
    }

    @Override
    public Optional<List<Programme>> findAll() {
        return Optional.ofNullable(programmeRepository.findAll());
    }

    @Override
    public void delete(String id) {
     programmeRepository.deleteById(id);
    }
}
