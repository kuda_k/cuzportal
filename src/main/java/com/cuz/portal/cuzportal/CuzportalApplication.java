package com.cuz.portal.cuzportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuzportalApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuzportalApplication.class, args);
	}

}

