package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.FinancialStatement;
import com.cuz.portal.cuzportal.document.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FinancialStatementRepository extends MongoRepository<FinancialStatement,String> {
    FinancialStatement findFinancialStatementByStudent(Student student);
}
