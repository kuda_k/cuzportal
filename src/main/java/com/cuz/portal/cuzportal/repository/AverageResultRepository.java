package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.AverageResult;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AverageResultRepository extends MongoRepository<AverageResult,String> {

    AverageResult findByRegNum(String regNum);
}
