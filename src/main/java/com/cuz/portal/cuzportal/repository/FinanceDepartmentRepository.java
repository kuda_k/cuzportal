package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.FinanceDepartment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FinanceDepartmentRepository extends MongoRepository<FinanceDepartment,String> {
}
