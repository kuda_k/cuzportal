package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.Programme;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProgrammeRepository extends MongoRepository<Programme,String> {
}
