package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends MongoRepository<Payment,String> {
}
