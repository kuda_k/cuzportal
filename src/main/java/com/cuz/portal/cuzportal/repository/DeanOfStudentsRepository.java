package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.DeanOfStudents;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeanOfStudentsRepository extends MongoRepository<DeanOfStudents,String> {
    DeanOfStudents findByName(String name);
}
