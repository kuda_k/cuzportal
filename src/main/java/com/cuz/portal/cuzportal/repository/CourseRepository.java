package com.cuz.portal.cuzportal.repository;


import com.cuz.portal.cuzportal.document.Course;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends MongoRepository<Course,String> {
}
