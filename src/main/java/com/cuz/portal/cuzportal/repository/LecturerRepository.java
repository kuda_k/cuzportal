package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.Lecturer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LecturerRepository extends MongoRepository<Lecturer,String> {
    Lecturer findLecturerByLecturerNumber(String lecturerNumber);

}
