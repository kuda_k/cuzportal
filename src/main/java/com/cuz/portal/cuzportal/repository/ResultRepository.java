package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.Result;
import com.cuz.portal.cuzportal.document.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends MongoRepository<Result,String> {
    List<Result> findResultsByStudent(Student student);

}
