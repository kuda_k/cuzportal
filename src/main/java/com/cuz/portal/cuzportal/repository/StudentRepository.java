package com.cuz.portal.cuzportal.repository;

import com.cuz.portal.cuzportal.document.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends MongoRepository<Student,String> {

    Student findStudentByRegNum(String regNum);
}
